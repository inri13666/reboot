#include<cstdlib>
#include<iostream>
#include <windows.h>
#include <stdio.h>
#include <tchar.h>

using namespace std;

int usage(char *argv[])
{
	printf("Usage:\n%s --force [timeout=0]\n%s --version\n", argv[0],argv[0]);
	return 0;
}
int version(char *argv[])
{
	printf("%s version 0.1\n", argv[0]);
	return 0;

}
int main(int argc, char *argv[])
{
	STARTUPINFO si;
    PROCESS_INFORMATION pi;
	char *str = new char[255];
	char *app = new char[255];

    ZeroMemory( &si, sizeof(si) );
    ZeroMemory( &pi, sizeof(pi) );
    ZeroMemory( str, sizeof(str) );
    ZeroMemory( app, sizeof(app) );

    si.cb = sizeof(si);

	strcpy(app,((const char*)"shutdown "));
	strcpy(str,((const char*)(strcat(app,"/r "))));
	switch(argc)
	{
		case 2:
			if(strcmp(argv[1],"--help")==0)
			{
				return usage(argv);
			}
			if(strcmp(argv[1],"--version")==0)
			{
				return version(argv);
			}
			if(strcmp(argv[1],"--force")==0)
			{
				strcat(str,((const char*)"/f /t 0"));
			}
			else
			{
				int _t = atoi(argv[1]);
				if(_t>=0)
				{
					strcat(str,((const char*)"/t "));
					strcat(str,argv[1]);
				}
			}
		break;
		case 3:
			int _t;
			_t = atoi(argv[2]);
			if((strcmp(argv[1],"--force")==0)&&(_t>=0))
			{
				strcat(str,((const char*)"/f /t "));
				strcat(str,argv[2]);
			}
			else
			{
				return usage(argv);
			}
		break;
		default:
			return usage(argv);
		break;
	}

    if( !CreateProcess( NULL,str,NULL,NULL,FALSE,0,NULL,NULL,&si,&pi))
    {
        printf( "Reboot failed (%d).\n", GetLastError() );
        return 1;
    }

    WaitForSingleObject( pi.hProcess, INFINITE );

    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
	return 0;
};
